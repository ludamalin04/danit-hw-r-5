import PropTypes from "prop-types";

const Close = ({click}) => {
    return (
        <div className='modal-header' onClick={click}>
            <img src="../../public/imgs/Icon.png" alt='close-icon'/>
        </div>
    );
};

export default Close;

Close.propTypes = {
    click: PropTypes.func
}