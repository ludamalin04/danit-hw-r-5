import PropTypes from "prop-types";

const ModalFooter = ({children}) => {
    return (
        <div className='modal-footer'>
            {children}
        </div>
    );
};

export default ModalFooter;

ModalFooter.propTypes = {
    children: PropTypes.any
}