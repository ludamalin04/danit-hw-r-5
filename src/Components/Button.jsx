import PropTypes from "prop-types";

const Button = (props) => {
    const {type, classNames, click, children, disabled} = props
    return (
        <>
            <button disabled={disabled} onClick={click} className={classNames} type={type}>{children}</button>
        </>
    )
}
export default Button

Button.defaultProps = {
    type: "button",
    disabled: false
}

Button.propTypes = {
    type: PropTypes.string,
    classNames: PropTypes.string,
    click: PropTypes.func,
    children: PropTypes.any,
    disabled: PropTypes.bool
}
