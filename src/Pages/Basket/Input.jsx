import {ErrorMessage, Field} from "formik";
import PropTypes from "prop-types";

const Input = ({label, type, name, placeholder}) => {
    return (
        <div className='input-section'>
            <label>
                <p className='label'>{label}</p>
                <Field className='input' type={type} name={name} placeholder={placeholder}/>
                <ErrorMessage className='error' name={name} component='p'/>
            </label>
        </div>
    );
};
Input.defaultProps = {
    type: 'text'
}
Input.propTypes = {
    label: PropTypes.string,
    name: PropTypes.string,
    placeholder: PropTypes.string,
    type: PropTypes.string
}


export default Input;