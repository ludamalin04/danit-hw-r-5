import {Formik, Form} from "formik";
import './Basket.scss'
import Input from "./Input.jsx";
import Button from "../../Components/Button.jsx";
import * as Yup from 'yup'
import PropTypes from "prop-types";

const FormSection = ({shop, buyBtn}) => {
    // const clearShop = () => {
    //     localStorage.removeItem('shop');
    // }
    // const clearBuyBtn = () => {
    //     localStorage.removeItem('buyBtn');
    //     alert(`З кошика видалено товари з артикулом ${buyBtn}`)
    // }
    return (
        <Formik
            initialValues={{
                name: '',
                surname: '',
                age: '',
                country: '',
                city: '',
                street: '',
                houseApt: '',
                phoneNumber: ''
            }}
            validationSchema={Yup.object({
                name: Yup.string()
                    .required('Необхідне для заповнення')
                    .matches(/^[\p{L}\s-]+$/u, 'Невірний формат імені'),
                surname: Yup.string()
                    .required('Необхідне для заповнення')
                    .matches(/^[\p{L}\s-]+$/u, 'Невірний формат прізвища'),
                age: Yup.number()
                    .required('Необхідне для заповнення')
                    .min(18, 'Невірний формат віку')
                    .max(99, 'Невірний формат віку'),
                country: Yup.string()
                    .required('Необхідне для заповнення')
                    .matches(/^[\p{L}\s-]+$/u, 'Невірний формат назви країни'),
                city: Yup.string()
                    .required('Необхідне для заповнення')
                    .matches(/^[\p{L}\s-]+$/u, 'Невірний формат назви міста'),
                street: Yup.string()
                    .required('Необхідне для заповнення')
                    .matches(/^[\p{L}\s\d-]+$/u, 'Невірний формат назви вулиці'),
                houseApt: Yup.string()
                    .required('Необхідне для заповнення')
                    .matches(/^\d{1,5}([,\/\s]\d{1,5})?$/, 'Невірний формат номера будинку/квартири'),
                phoneNumber: Yup.string()
                    .required('Необхідне для заповнення')
                    .matches(/^(\+|\d{1})\d{9,12}$/, 'Невірний формат номеру')
            })}
            onSubmit={values => {
                console.log('Дані з форми', values)
                console.log('Список покупок', shop)
                localStorage.removeItem('shop');
                localStorage.removeItem('buyBtn');
                alert(`З кошика видалено товари з артикулом ${buyBtn}`);
                window.location.reload();
            }}
        >
            <Form className='form'>
                <div className='form-section'>
                    <Input label="Ім'я" name='name' placeholder="Ваше і'мя"/>
                    <Input label='Прізвище' name='surname' placeholder='Ваше прізвище'/>
                    <Input label='Вік' type='number' name='age' placeholder='Ваш вік'/>
                    <Input label='Країна' name='country' placeholder='Країна'/>
                    <Input label='Місто' name='city' placeholder='Місто'/>
                    <Input label='Вулиця' name='street' placeholder='Вулиця'/>
                    <Input label='Номер будинку/квартири' name='houseApt' placeholder='Номер будинку/квартири'/>
                    <Input label='Номер телефону' name='phoneNumber' placeholder='Ваш номер телефону'/>
                </div>
                <Button classNames='modal-btn-active' type='submit'>Відправити</Button>
            </Form>
        </Formik>
    );
};
FormSection.propTypes = {
    shop: PropTypes.array,
    buyBtn: PropTypes.array,
}
export default FormSection;