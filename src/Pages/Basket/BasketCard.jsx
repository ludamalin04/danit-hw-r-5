import PropTypes from "prop-types";
import Close from "../../Components/Close.jsx";
import {useDispatch} from "react-redux";
import {actionModal} from "../../store/actions.js";
import Button from "../../Components/Button.jsx";

const BasketCard = ({
                        color,
                        article,
                        price,
                        name,
                        img,
                        alt,
                        quantity,
                        inc,
                        dec,
                        handleCurrentCard
                    }) => {

    const dispatch = useDispatch();

// відкриття і закриття модалки
    const actionFirst = () => {
        dispatch(actionModal());
    }

    const basketItem = {color, article, price, name, img, alt, quantity}
    return (
        <div className='shop-card'>
            <Close click={() => {
                actionFirst()
                handleCurrentCard(basketItem)
            }}/>
            <div>
                <img src={img} alt={alt}/>
            </div>
            <div className='shop-card-favorite'>
                <h3 className='shop-card-name'>{name}</h3>
                <div className='inc-dec-container'>
                    <Button classNames='inc-dec-btn'
                            click={() => {dec(basketItem)}}>-</Button>
                    <p>{quantity}</p>
                    <Button classNames='inc-dec-btn'
                            click={() => {inc(basketItem)}}>+</Button>
                </div>
            </div>
            <div className='shop-card-content'>
                <span className='shop-card-article'>артикул {article}</span>
                <h3 className='shop-card-color'>{color}</h3>
                <div className='shop-card-buy'>
                    <h3><span className='shop-card-price'>{price}</span>грн</h3>
                    <p className='total'>{Math.round(price * quantity*100)/100}</p>
                </div>
            </div>
        </div>
    );
};

BasketCard.propTypes = {
    color: PropTypes.string,
    handleCurrentCard: PropTypes.func,
    article: PropTypes.number,
    price: PropTypes.number,
    name: PropTypes.string,
    img: PropTypes.string,
    alt: PropTypes.string,
    quantity: PropTypes.number,
    children: PropTypes.any,
    inc: PropTypes.func,
    dec: PropTypes.func
}

export default BasketCard;