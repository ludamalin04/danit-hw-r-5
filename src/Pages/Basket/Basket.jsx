import PropTypes from "prop-types";
import BasketCard from "./BasketCard.jsx";
import Modal from "../../Components/Modal.jsx";
import ModalWrapper from "../../Components/ModalWrapper.jsx";
import ModalFooter from "../../Components/ModalFooter.jsx";
import Button from "../../Components/Button.jsx";
import {useDispatch, useSelector} from "react-redux";
import {actionModal} from "../../store/actions.js";
import {useEffect, useState} from "react";
import "./Basket.scss"
import FormSection from "./FormSection.jsx";

const Basket = ({
                    removeShop,
                    handleCurrentCard,
                    currentCard,
                    shop,
                    buyBtn,
                    buyClick,
                    setShop,
                }) => {

    const dispatch = useDispatch();

// відкриття і закриття модалки
    const isOpenFirst = useSelector((state) => state.isOpenFirst)
    const actionFirst = () => {
        dispatch(actionModal());
    }

// підрахунок загальної суми покупок
    const [price, setPrice] = useState(0);
    const handlePrice = () => {
        let total = 0;
        shop.map((item) => (
            total += item.quantity * item.price
        ))
        setPrice(total)
    }
    useEffect(() => {
        handlePrice();
    })

//робота каунтера
    const inc = (product) => {
        const exist = shop.find((x) => {
            return x.article === product.article
        })
        setShop(shop.map((item) => {
            return item.article === product.article ? {...exist, quantity: exist.quantity + 1} : item
        }))
    }
    const dec = (product) => {
        const exist = shop.find((x) => {
            return x.article === product.article
        })
        if (exist.quantity > 1) {
            setShop(shop.map((item) => {
                return item.article === product.article ? {...exist, quantity: exist.quantity - 1} : item
            }))
        }
    }

    return (
        <>
            {isOpenFirst && (
                <ModalWrapper click={actionFirst}>
                    <Modal><h2 className='basket-question'>Видалити товар?</h2>
                        <ModalFooter>
                            <Button click={actionFirst} classNames="modal-btn">СКАСУВАТИ</Button>
                            <Button click={() => {
                                actionFirst()
                                removeShop(currentCard.article)
                                buyClick(currentCard.article)
                            }}
                                    classNames="modal-btn-active"
                            >
                                ВИДАЛИТИ</Button>
                        </ModalFooter>
                    </Modal>
                </ModalWrapper>
            )}
            <div className='shop'>
                {
                    shop.map(({article, name, img, price, color, quantity}) => {
                        return (
                            <BasketCard
                                handleCurrentCard={handleCurrentCard}
                                buyBtn={buyBtn}
                                key={article}
                                alt={name}
                                img={img}
                                name={name}
                                article={article}
                                price={price}
                                color={color}
                                quantity={quantity}
                                inc={inc}
                                dec={dec}
                            >
                            </BasketCard>
                        )
                    })
                }
            </div>
            {shop.length === 0 && (
                <h1 className='basket-info'>Ваш кошик порожній</h1>
            )}
            {shop.length > 0 && (
                <>
                    <h2 className='basket-info'>Загальна сума покупок {Math.round(price * 100) / 100} грн</h2>
                    <FormSection shop={shop} buyBtn={buyBtn}></FormSection>
                </>
            )}

        </>
    );
};

Basket.propTypes = {
    shop: PropTypes.array,
    removeShop: PropTypes.func,
    handleCurrentCard: PropTypes.func,
    currentCard: PropTypes.object,
    buyBtn: PropTypes.array,
    buyClick: PropTypes.func,
    setShop: PropTypes.any,
}

export default Basket;