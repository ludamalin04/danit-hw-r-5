import Button from "../../Components/Button.jsx";

const HeaderHero = () => {
    return (
        <div className='header-hero'>
            <h1 className='header-name'>Blue Band</h1>
            <h2 className='header-title'>краса в кожному моменті</h2>
            <Button classNames='hero-btn'>до покупок</Button>
        </div>
    );
};

export default HeaderHero;