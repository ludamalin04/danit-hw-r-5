import "./Shop.scss";
import {useEffect} from "react";
import PropTypes from "prop-types";
import HeaderHero from "./HeaderHero.jsx";
import ShopCard from "./ShopCard.jsx";
import Modal from "../../Components/Modal.jsx";
import ModalWrapper from "../../Components/ModalWrapper.jsx";
import ModalBody from "../../Components/ModalBody.jsx";
import Button from "../../Components/Button.jsx";
import ModalFooter from "../../Components/ModalFooter.jsx";
import {useDispatch, useSelector} from "react-redux";
import {fetchData, actionModal} from "../../store/actions.js";

const Shop = ({
                  children,
                  handleCurrentCard,
                  currentCard,
                  buyBtn,
                  buyClick,
                  handleFavorite,
                  handleShop,
                  favHeart,
                  colorClick
              }) => {

    const dispatch = useDispatch()

// рендер карток з store
    const data = useSelector((store) => store.data);
    useEffect(() => {
        dispatch(fetchData())
    }, [])

// відкриття і закриття модалки
    const isOpenFirst = useSelector((state) => state.isOpenFirst)
    const actionFirst = () => {
        dispatch(actionModal());
    }

// збереження зміни кольору серця в localstorage
    useEffect(() => {
        localStorage.setItem('favHeart', JSON.stringify(favHeart));
    }, [favHeart]);

    return (
        <>
            <HeaderHero/>
            {isOpenFirst && (
                <ModalWrapper click={actionFirst}>
                    <Modal handleShop={handleShop}>
                        <ModalBody
                            url={currentCard.img}
                            title={currentCard.name}
                            desc={currentCard.price}
                            article={currentCard.article}>
                        </ModalBody>
                        <ModalFooter>
                            <Button click={actionFirst} classNames="modal-btn">СКАСУВАТИ</Button>
                            <Button click={() => {
                                actionFirst()
                                handleShop(currentCard)
                                buyClick(currentCard.article)
                            }}
                                    classNames="modal-btn-active">
                                ДОДАТИ В КОШИК</Button>
                        </ModalFooter>
                    </Modal>
                </ModalWrapper>
            )}
            <div className='shop'>
                {
                    data.map(({article, name, url, price, color}) => {
                        return (
                            <ShopCard
                                handleFavorite={handleFavorite}
                                handleCurrentCard={handleCurrentCard}
                                currentCard={currentCard}
                                colorClick={colorClick}
                                buyBtn={buyBtn}
                                favHeart={favHeart}
                                key={article}
                                alt={name}
                                img={url}
                                name={name}
                                article={article}
                                price={price}
                                color={color}
                            >
                            </ShopCard>
                        )
                    })
                }
                {children}</div>
        </>
    )
}

Shop.propTypes = {
    children: PropTypes.any,
    handleCurrentCard: PropTypes.func,
    currentCard: PropTypes.object,
    favHeart: PropTypes.any,
    colorClick: PropTypes.func,
    handleFavorite: PropTypes.func,
    buyBtn: PropTypes.any,
    buyClick: PropTypes.func,
    handleShop: PropTypes.func,
}
export default Shop;