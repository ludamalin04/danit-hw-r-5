import PropTypes from "prop-types";
import Heart from "../../Components/Heart.jsx";

const FavoriteCard = ({
                          color,
                          article,
                          price,
                          name,
                          img,
                          alt,
                          colorClick,
                          favHeart,
                          removeFavorite,
                      }) => {
    return (
        <div className='shop-card'>
            <div>
                <img src={img} alt={alt}/>
            </div>
            <div className='shop-card-favorite'>
                <h3 className='shop-card-name'>{name}</h3>
                <Heart
                    article={article}
                    classNames={favHeart.includes(article) ? "favheart" : ""}
                    click={() => {
                        colorClick(article)
                        removeFavorite(article)
                    }}
                />
            </div>
            <div className='shop-card-content'>
                <span className='shop-card-article'>артикул {article}</span>
                <h3 className='shop-card-color'>{color}</h3>
                <div className='shop-card-buy'>
                    <h3><span className='shop-card-price'>{price}</span>грн</h3>
                </div>
            </div>
        </div>
    );
};

FavoriteCard.propTypes = {
    color: PropTypes.string,
    article: PropTypes.number,
    price: PropTypes.number,
    name: PropTypes.string,
    img: PropTypes.string,
    alt: PropTypes.string,
    favHeart: PropTypes.any,
    colorClick: PropTypes.func,
    removeFavorite: PropTypes.func
}

export default FavoriteCard;