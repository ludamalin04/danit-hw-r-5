import PropTypes from "prop-types";
import FavoriteCard from "./FavoriteCard.jsx";

const Favorites = ({
                       colorClick,
                       favHeart,
                       favorite,
                       removeFavorite
                   }) => {

    return (
        <>
            <div className='shop'>
                {
                    favorite.map(({article, name, img, price, color}) => {
                        return (
                            <FavoriteCard
                                key={article}
                                alt={name}
                                img={img}
                                name={name}
                                article={article}
                                price={price}
                                color={color}
                                favHeart={favHeart}
                                colorClick={colorClick}
                                removeFavorite={removeFavorite}
                            ></FavoriteCard>
                        )
                    })
                }
                {favorite.length === 0 && (
                    <h1>У вас немає вибраних товарів</h1>
                )}
            </div>
        </>
    );
};

Favorites.propTypes = {
    favorite: PropTypes.array,
    favHeart: PropTypes.any,
    colorClick: PropTypes.func,
    removeFavorite: PropTypes.func
}

export default Favorites;