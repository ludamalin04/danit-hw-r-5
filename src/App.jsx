import {Routes, Route} from "react-router-dom";
import {useState, useEffect} from "react";
import Shop from "./Pages/Shop/Shop.jsx";
import Nav from "./Components/Nav.jsx";
import Favorites from "./Pages/Favorites/Favorites.jsx";
import Basket from "./Pages/Basket/Basket.jsx";

function App() {

    const shopFromLocalStorage = JSON.parse(localStorage.getItem('shop')) || [];
    const buyBtnFromLocalStorage = JSON.parse(localStorage.getItem('buyBtn')) || [];
    const favoriteFromLocalStorage = JSON.parse(localStorage.getItem('favorite')) || [];
    const favHeartFromLocalStorage = JSON.parse(localStorage.getItem('favHeart')) || [];

// відображення інфо з картки у модалці
    const [currentCard, setCurrentCard] = useState({});
    const handleCurrentCard = (card) => setCurrentCard(card);

// додавання у вибране
    const [favorite, setFavorite] = useState(favoriteFromLocalStorage);
    const handleFavorite = (item) => {
        const isFavorite = favorite.some((fav) => fav.article === item.article)
        if (isFavorite) {
            const existFav = favorite.filter((fav) => fav.article !== item.article);
            setFavorite(existFav);
        } else {
            setFavorite([...favorite, item]);
        }
    };

// видалення з вибраного
    const removeFavorite = (productArticle) => {
        const updatedFavorite = favorite.filter((item) => item.article !== productArticle);
        setFavorite(updatedFavorite)
    }

// збереження вибраного в localstorage
    useEffect(() => {
        localStorage.setItem('favorite', JSON.stringify(favorite));
    }, [favorite]);

// зміна кольору серця
    const [favHeart, setFavHeart] = useState(favHeartFromLocalStorage);
    const colorClick = (article) => {
        setFavHeart((prev) => {
            return prev.includes(article) ? prev.filter((number) => number !== article) : [...prev, article]
        })
    };

// // додавання у кошик
    const [shop, setShop] = useState(shopFromLocalStorage);
    const handleShop = (item) => {
        setShop([...shop, {...item, quantity: 1}]);
    };

// видалення з кошику
    const removeShop = (productArticle) => {
        const updatedShop = shop.filter((item) => item.article !== productArticle);
        setShop(updatedShop);
    }

// збереження кошика в localstorage
    useEffect(() => {
        localStorage.setItem('shop', JSON.stringify(shop));
    }, [shop]);

// зміна кольору кнопки
    const [buyBtn, setBuyBtn] = useState(buyBtnFromLocalStorage);
    const buyClick = (article) => {
        setBuyBtn((prev) => {
            return prev.includes(article)
                ? prev.filter((number) => number !== article)
                : [...prev, article]
        })
    }

// збереження зміни кольору кнопки в localstorage
    useEffect(() => {
        localStorage.setItem('buyBtn', JSON.stringify(buyBtn));
    }, [buyBtn]);

    return (
        <>
            <Nav shop={shop} favorite={favorite}></Nav>
            <Routes>
                <Route path='/' element={
                    <Shop
                        buyBtn={buyBtn}
                        buyClick={buyClick}
                        handleShop={handleShop}
                        handleFavorite={handleFavorite}
                        handleCurrentCard={handleCurrentCard}
                        currentCard={currentCard}
                        favHeart={favHeart}
                        colorClick={colorClick}
                    ></Shop>
                }></Route>
                <Route path='/basket' element={
                    <Basket shop={shop}
                            removeShop={removeShop}
                            handleCurrentCard={handleCurrentCard}
                            currentCard={currentCard}
                            buyClick={buyClick}
                            buyBtn={buyBtn}
                            setShop={setShop}
                    ></Basket>}></Route>
                <Route path='/favorites' element={
                    <Favorites
                        favorite={favorite}
                        favHeart={favHeart}
                        colorClick={colorClick}
                        removeFavorite={removeFavorite}
                    ></Favorites>}>
                </Route>
            </Routes>
        </>
    )
}

export default App
